#!/bin/bash

if [ -z "$1" ]; then
	echo "usage: $0 name"
	exit 1
fi
sudo touch /etc/apache2/sites-available/$1.conf
cat ./basic_symfony.conf | sudo tee -a /etc/apache2/sites-available/$1.conf
sudo perl -pi -e "s/blog/$1/g" /etc/apache2/sites-available/$1.conf
#Optional
sudo rm /etc/apache2/sites-enabled/*
sudo a2ensite $1.conf 
sudo service apache2 restart
