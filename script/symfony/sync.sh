#!/bin/bash

path_vagrant='/vagrant/html'
path_www='/var/www/html'

if [ $# -eq 1 ]; then
	path_vagrant="$path_vagrant/$1"
	path_www="$path_www/$1"
fi

sudo unison -auto $path_vagrant $path_www;
